# vim: set cc=80 et ts=4 sts=0 sw=0:

SRCDIR := src
OBJDIR := obj
LIBOBJDIR := libobj
LIBDIR := lib
XLIBDIR := extralib

DEBUG := -g
OPTIMIZE := -O0
CFLAGS := $(DEBUG) $(OPTIMIZE) -std=gnu99 -Wall -pedantic
LDFLAGS := -L$(LIBDIR) -L$(XLIBDIR)

.PHONY: help all clean
.PRECIOUS: $(LIBOBJDIR)/%.o

help:
	$(info VARIABLES)
	$(info ===================================================================)
	$(info SRCDIR:      Source directory.)
	$(info OBJDIR:      Object directory.)
	$(info LIBOBJDIR:   Library object directory.)
	$(info LIBDIR:      Library directory.)
	$(info XLIBDIR:     Extra (external) library directory.)
	$(info DEBUG:       Embed debugging information.)
	$(info OPTIMIZE:    Enable compiler optimizations.)
	$(info CC:          Compiler to use.)
	$(info CFLAGS:      Compiler flags.)
	$(info LDFLAGS:     Linker flags.)
	$(info )
	$(info RULES)
	$(info ===================================================================)
	$(info help:         Display this help menu.)
	$(info all:          Build object files and link them into executable.)
	$(info psclient:     Build the client program.)
	$(info psserver:     Build the server program.)
	$(info clean:        Remove files produced by the building process.)

all: psclient psserver

psclient: $(OBJDIR)/psclient.o $(LIBDIR)/libaux.so
	$(CC) $(LDFLAGS) -o $@ '$<' -pthread -laux

psserver: $(XLIBDIR)/libcsse2310a4.so
psserver: $(OBJDIR)/psserver.o $(LIBDIR)/libaux.so $(LIBDIR)/libstringmap.so
	$(CC) $(LDFLAGS) -o $@ '$<' -pthread -laux -lstringmap -lcsse2310a4

$(OBJDIR) $(LIBOBJDIR) $(LIBDIR):
	mkdir -p '$@'

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)
	$(CC) $(CFLAGS) -c -o '$@' '$<'

$(LIBOBJDIR)/%.o: $(SRCDIR)/%.c | $(LIBOBJDIR)
	$(CC) -fPIC $(CFLAGS) -c -o '$@' '$<'

$(LIBDIR)/lib%.so: $(LIBOBJDIR)/%.o | $(LIBDIR)
	$(CC) -shared $(LDFLAGS) -o '$@' '$<'

clean:
	rm -rf psclient psserver '$(OBJDIR)' '$(LIBOBJDIR)' '$(LIBDIR)'
