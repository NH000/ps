/*
 * vim: set cc=80 et ts=4 sts=0 sw=0:
 *
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of PS.
 *
 * PS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PS.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include "stringmap.h"

/* SEE THE HEADER FILE FOR INFO */

#define MAP_SIZE    1024

typedef struct StringMapBucket StringMapBucket;

struct StringMapBucket {
    StringMapItem item;
    StringMapBucket *next;
};

struct StringMap {
    StringMapBucket *buckets[MAP_SIZE];
};

// Uses sdbm algorithm.
// Reference: https://stackoverflow.com/a/14409947/18252519.
static size_t stringmap_hash(const char *key) {
    size_t hash = 0;
    for (int i = 0; key[i] != '\0'; i++) {
        hash = key[i] + (hash << 6) + (hash << 16) - hash;
    }

    return hash;
}

StringMap *stringmap_init() {
    return calloc(1, sizeof(StringMap));
}

void stringmap_free(StringMap *sm) {
    if (!sm) {
        return;
    }

    for (int i = 0; i < MAP_SIZE; i++) {
        for (StringMapBucket *bucket = sm->buckets[i]; bucket;) {
            free(bucket->item.key);

            StringMapBucket *nextBucket = bucket->next;
            free(bucket);
            bucket = nextBucket;
        }
    }

    free(sm);
}

void *stringmap_search(StringMap *sm, char *key) {
    if (!sm || !key) {
        return NULL;
    }

    for (StringMapBucket *bucket = sm->buckets[stringmap_hash(key) % MAP_SIZE];
            bucket; bucket = bucket->next) {
        if (!strcmp(bucket->item.key, key)) {
            return bucket->item.item;
        }
    }

    return NULL;
}

int stringmap_add(StringMap *sm, char *key, void *item) {
    if (!sm || !key) {
        return 0;
    }

    StringMapBucket **bucket;
    for (bucket = sm->buckets + stringmap_hash(key) % MAP_SIZE; *bucket;
            bucket = &(*bucket)->next) {
        if (!strcmp((*bucket)->item.key, key)) {
            return 0;
        }
    }

    StringMapBucket *newBucket = calloc(1, sizeof(StringMapBucket));
    if (!newBucket || !(newBucket->item.key = strdup(key))) {
        free(newBucket);
        return 0;
    }
    newBucket->item.item = item;

    *bucket = newBucket;

    return 1;
}

int stringmap_remove(StringMap *sm, char *key) {
    if (!sm || !key) {
        return 0;
    }

    StringMapBucket **bucket;
    for (bucket = sm->buckets + stringmap_hash(key) % MAP_SIZE; *bucket;
            bucket = &(*bucket)->next) {
        if (!strcmp((*bucket)->item.key, key)) {
            break;
        }
    }

    if (!*bucket) {
        return 0;
    }

    StringMapBucket *bucketCopy = *bucket;
    *bucket = (*bucket)->next;
    free(bucketCopy->item.key);
    free(bucketCopy);

    return 1;
}

StringMapItem *stringmap_iterate(StringMap *sm, StringMapItem *prev) {
    if (!sm || (prev && !prev->key)) {
        return NULL;
    }

    if (!prev) {
        for (int i = 0; i < MAP_SIZE; i++) {
            if (sm->buckets[i]) {
                return &sm->buckets[i]->item;
            }
        }

        return NULL;
    }

    for (int i = stringmap_hash(prev->key) % MAP_SIZE; i < MAP_SIZE; i++) {
        for (StringMapBucket *bucket = sm->buckets[i]; bucket;
                bucket = bucket->next) {
            if (!memcmp(&bucket->item, prev, sizeof(StringMapItem))) {
                if (bucket->next) {
                    return &bucket->next->item;
                }

                while (++i < MAP_SIZE) {
                    if (sm->buckets[i]) {
                        return &sm->buckets[i]->item;
                    }
                }

                return NULL;
            }
        }
    }

    return NULL;
}
