/*
 * vim: set cc=80 et ts=4 sts=0 sw=0:
 *
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of PS.
 *
 * PS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PS.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

/* SEE THE HEADER FILE FOR INFO */

char *string_concat_multiple(int strings_n, ...) {
    va_list strings;

    va_start(strings, strings_n);
    size_t result_s = 0;
    for (int i = 0; i < strings_n; i++) {
        result_s += strlen(va_arg(strings, const char *));
    }
    va_end(strings);

    char *result = malloc(result_s + 1);
    if (result) {
        result[0] = '\0';

        va_start(strings, strings_n);
        for (int i = 0; i < strings_n; i++) {
            result = strcat(result, va_arg(strings, const char *));
        }
        va_end(strings);
    }

    return result;
}

bool is_all_digits(const char *str) {
    if (!str || !*str) {
        return false;
    }

    for (const char *p = str; *p; p++) {
        if (!isdigit(*p)) {
            return false;
        }
    }

    return true;
}
