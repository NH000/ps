/*
 * vim: set cc=80 et ts=4 sts=0 sw=0:
 *
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of PS.
 *
 * PS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PS.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <error.h>
#include <sys/socket.h>
#include <netdb.h>
#include <pthread.h>

#include "const.h"
#include "aux.h"

// Server IP address.
#define SERVER_ADDRESS "localhost"

// Exit values of the program.
enum ExitCode {
    EXIT_CODE_SUCCESS,          // No errors.
    EXIT_CODE_SMALL_CMD,        // Not enough arguments on the command-line.
    EXIT_CODE_BAD_CMD,          // Invalid arguments on the command-line.
    EXIT_CODE_CONN_FAIL,        // Failed to connect to the server.
    EXIT_CODE_CONN_TERM,        // Server terminated the connection.
};

// Error message format strings.
#define ERROR_MSG_SMALL_CMD "Usage: %s portnum name [topic] ..."
#define ERROR_MSG_BAD_CMD   "invalid %s"
#define ERROR_MSG_CONN_FAIL "unable to connect to port %s"
#define ERROR_MSG_CONN_TERM "server connection terminated"

// Command-line arguments as a data structure.
struct CMDArgs {
    const char *port;           // Service name / port number to connect to.
    const char *name;           // Name of the client program.
    const char *const *topics;  // Topics the client initially wants to be
                                // subscribed to.
    int topicsN;                // Number of entries in the topics field.
};

/*
 * Returns whether the given command-line argument is of the valid format, as
 * determined by the supplied type.
 *
 * type: Type of the argument, used to check its format. Currently valid values
 *       are: "name"; "topic".
 * arg: Argument to test for format validity.
 *
 * Returns: Validity of the supplied argument with respect to the supplied
 *          argument type.
 */
bool is_cmd_arg_valid(const char *type, const char *arg) {
    if (!strcmp(type, "name") || !strcmp(type, "topic")) {
        if (*arg == '\0') {
            return false;
        }

        for (const char *p = arg; *p != '\0'; p++) {
            if (*p == ':' || *p == ' ' || *p == '\n') {
                return false;
            }
        }

        return true;
    }

    return false;
}

/*
 * Parses the given command-line arguments and stores them in the supplied data
 * structure. Exits the program on error.
 *
 * cmdArgs: Structure to store the parsed command-line arguments into.
 * argc: Argument count (received from `main()`).
 * argv: Argument vector (received from `main()`).
 *
 * Errors: EXIT_CODE_SMALL_CMD; EXIT_CODE_BAD_CMD.
 */
void process_cmd_args(struct CMDArgs *cmdArgs, int argc,
        const char *const *argv) {
    if (argc < 3) {
        fprintf(stderr, ERROR_MSG_SMALL_CMD "\n", argv[0]);
        exit(EXIT_CODE_SMALL_CMD);
    }

    cmdArgs->port = argv[1];

    if (!is_cmd_arg_valid("name", argv[2])) {
        error(EXIT_CODE_BAD_CMD, 0, ERROR_MSG_BAD_CMD, "name");
    }

    cmdArgs->name = argv[2];

    for (int t = 3; t < argc; t++) {
        if (!is_cmd_arg_valid("topic", argv[t])) {
            error(EXIT_CODE_BAD_CMD, 0, ERROR_MSG_BAD_CMD, "topic");
        }
    }

    cmdArgs->topics = argv + 3;
    cmdArgs->topicsN = argc - 3;
}

/*
 * Establishes a TCP connection with the given server, using the supplied 
 * service name / port number. Exits the program on error.
 *
 * server: Address of the server to connect to.
 * port: Service name / port number to connect to the server on.
 *
 * Returns: Socket to use for the communication with the server.
 *
 * Errors: EXIT_CODE_CONN_FAIL.
 */
int server_connect(const char *server, const char *port) {
    // Discover the server.
    struct addrinfo *serverAddr;
    struct addrinfo serverSpec = { .ai_family = AF_INET,
            .ai_socktype = SOCK_STREAM, .ai_protocol = 0 };
    if (getaddrinfo(server, port, &serverSpec, &serverAddr)) {
        freeaddrinfo(serverAddr);
        error(EXIT_CODE_CONN_FAIL, 0, ERROR_MSG_CONN_FAIL, port);
    }

    // Create the socket to communicate with the server.
    int socketFD = socket(AF_INET, SOCK_STREAM, 0);
    
    // Connect to the server via previously created socket.
    if (connect(socketFD, serverAddr->ai_addr, serverAddr->ai_addrlen)) {
        close(socketFD);
        freeaddrinfo(serverAddr);
        error(EXIT_CODE_CONN_FAIL, 0, ERROR_MSG_CONN_FAIL , port);
    }

    freeaddrinfo(serverAddr);

    return socketFD;
}

/*
 * "Registers" this client to the connected server: sends the name and the
 * initially subscribed-to topics list as they were supplied on the
 * command-line. Exits the program on error.
 *
 * socketFD: Socket used for communication with the server.
 * cmdArgs: Parsed command-line arguments.
 *
 * Errors: EXIT_CODE_CONN_TERM.
 */
void server_register(int socketFD, const struct CMDArgs *cmdArgs) {
    // Send the "name" command first...
    char *data = string_concat_multiple(3, "name ", cmdArgs->name, "\n");
    if (send(socketFD, data, strlen(data), 0) == -1) {
        free(data);
        close(socketFD);
        error(EXIT_CODE_CONN_TERM, 0, ERROR_MSG_CONN_TERM);
    }
    free(data);

    // ...then a "sub" command for each of the listed topics.
    for (int t = 0; t < cmdArgs->topicsN; t++) {
        data = string_concat_multiple(3, "sub ", cmdArgs->topics[t], "\n");
        if (send(socketFD, data, strlen(data), 0) == -1) {
            free(data);
            close(socketFD);
            error(EXIT_CODE_CONN_TERM, 0, ERROR_MSG_CONN_TERM);
        }
        free(data);
    }
}

/*
 * Sends data from STDIN to the server. To be run within a dedicated thread.
 * Exits the program on error.
 *
 * arg: Pointer to the socket used for communication with the server.
 *
 * Errors: EXIT_CODE_CONN_TERM.
 */
void *server_send(void *arg) {
    int *socketFD = arg;

    char data[NETWORK_BUFFER_SIZE];
    while (fgets(data, sizeof(data), stdin)) {
        if (send(*socketFD, data, strlen(data), 0) == -1) {
            error(EXIT_CODE_CONN_TERM, 0, ERROR_MSG_CONN_TERM);
        }
    }

    return NULL;
}

/*
 * Receives data from the server onto STDOUT. To be run within a dedicated
 * thread. Exits the program on error, which is the only way this function can
 * exit.
 *
 * arg: Pointer to the socket used for communication with the server.
 *
 * Errors: EXIT_CODE_CONN_TERM.
 */
void *server_recv(void *arg) {
    int *socketFD = arg;

    char data[NETWORK_BUFFER_SIZE];
    ssize_t countRecv;
    while ((countRecv = recv(*socketFD, data, sizeof(data) - 1, 0)) >= 1) {
        data[countRecv] = '\0';
        fputs(data, stdout);
    }

    error(EXIT_CODE_CONN_TERM, 0, ERROR_MSG_CONN_TERM);

    return NULL;
}

/*
 * Runs and manages the threads for sending data to and receiving it from the
 * server. Exits the program on error.
 *
 * socketFD: Socket used for communication with the server.
 *
 * Errors: EXIT_CODE_CONN_TERM.
 */
void server_communication(int socketFD) {
    pthread_t threadSend, threadRecv;
    pthread_create(&threadSend, NULL, &server_send, &socketFD);
    pthread_create(&threadRecv, NULL, &server_recv, &socketFD);

    pthread_join(threadSend, NULL);
    pthread_cancel(threadRecv);
    pthread_join(threadRecv, NULL);
}

int main(int argc, char *argv[]) {
    // Parse the command-line arguments and store them in the dedicated
    // data structure.
    struct CMDArgs cmdArgs;
    process_cmd_args(&cmdArgs, argc, (const char *const *) argv);

    // Establish a TCP connection with the server.
    int socketFD = server_connect(SERVER_ADDRESS, cmdArgs.port);

    // Register this client on the server.
    server_register(socketFD, &cmdArgs);

    // Start exchanging data with the server.
    server_communication(socketFD);

    // Close the client socket.
    close(socketFD);

    return EXIT_CODE_SUCCESS;
}
