/*
 * vim: set cc=80 et ts=4 sts=0 sw=0:
 *
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of PS.
 *
 * PS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PS.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <error.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <pthread.h>

#include "const.h"
#include "aux.h"
#include "stringmap.h"
#include "csse2310a4.h"

// Number of allowed pending client connection requests.
#define LISTENING_BACKLOG   10

// Starting capacity for the topic map items.
#define TOPIC_MAP_ITEM_START_CAPACITY   8

// Exit values of the program.
enum ExitCode {
    EXIT_CODE_SUCCESS,          // No errors.
    EXIT_CODE_BAD_CMD,          // Invalid arguments on the command-line.
    EXIT_CODE_LIST_FAIL,        // Socket listening error.
    EXIT_CODE_HTTP_LIST_FAIL    // HTTP socket listening error.
};

#define ERROR_MSG_BAD_CMD           "Usage: %s connections [portnum]"
#define ERROR_MSG_LIST_FAIL         "unable to open socket for listening"
#define ERROR_MSG_HTTP_LIST_FAIL    "unable to open HTTP socket for listening"

// Types of the commands the server may receive (self-explanatory).
enum ServerCMDType {
    SERVER_CMD_NONE,
    SERVER_CMD_NAME,
    SERVER_CMD_SUB,
    SERVER_CMD_UNSUB,
    SERVER_CMD_PUB,
};

// Describes a server command.
struct ServerCMD {
    enum ServerCMDType type;    // Type of the command.
    char *name;                 // Name parameter.
    char *value;                // Value parameter.
};

// Command-line arguments as a data structure.
struct CMDArgs {
    int connectionsN;   // Number of allowed client connections.
    int portNumber;     // Port number on which to listen for connections.
};

// Program statistics.
struct Stats {
    size_t connNow;     // Number of active client connections.
    size_t connTotal;   // Number of total client connections from the program
                        // start.
    size_t subOps;      // Number of "sub" commands executed.
    size_t unsubOps;    // Number of "unsub" commands executed.
    size_t pubOps;      // Number of "pub" commands executed.

    pthread_mutex_t lockModStats;   // Whether the program statistics can be
                                    // accessed.
};

// List of subscribed clients for a topic.
struct TopicMapItem {
    int *socketFD;      // The list itself.
    size_t clientsN;    // Number of subscribed clients.
    size_t capacity;    // In-memory size of the list.
};

// Data supplied to client threads.
struct ClientThreadData {
    int socketFD;                   // Client socket.
    StringMap *topicMap;            // Mapping between topics and their
                                    // subscribed clients.
    int connectionsN;               // Number of allowed client connections.
    struct Stats *stats;            // Program statistics.
    pthread_mutex_t *lockTopicMap;  // Whether the topic map can be
                                    // manipulated.
    pthread_mutex_t *lockAccConn;   // Whether a new connection can be
                                    // accepted.
};

// Data needed for serving HTTP connections.
struct HTTPData {
    int socketFD;         // Client socket.
    struct Stats *stats;  // Program statistics.
};

/*
 * Parses the given command-line arguments and stores them in the supplied data
 * structure. Exits the program on error.
 *
 * cmdArgs: Structure to store the parsed command-line arguments into.
 * argc: Argument count (received from `main()`).
 * argv: Argument vector (received from `main()`).
 *
 * Errors: EXIT_CODE_BAD_CMD.
 */
void process_cmd_args(struct CMDArgs *cmdArgs, int argc,
        const char *const *argv) {
    if (argc < 2 || argc > 3 || !is_all_digits(argv[1])
            || (argc == 3 && (!*argv[2] || !is_all_digits(argv[2])))) {
        fprintf(stderr, ERROR_MSG_BAD_CMD "\n", argv[0]);
        exit(EXIT_CODE_BAD_CMD);
    }

    cmdArgs->connectionsN = atoi(argv[1]);
    cmdArgs->portNumber = argc == 3 && argv[2][0] ? atoi(argv[2]) : 0;

    if (cmdArgs->portNumber != 0 && (cmdArgs->portNumber < 1024
            || cmdArgs->portNumber > 65535)) {
        fprintf(stderr, ERROR_MSG_BAD_CMD "\n", argv[0]);
        exit(EXIT_CODE_BAD_CMD);
    }
}

/*
 * Subscribes the given client to the given topic, creating it if needed.
 *
 * topicMap: Topic map.
 * socketFD: Client to subscribe.
 * topic: Topic for subscription.
 *
 * Returns: Was the subscription carried out (i.e. it wasn't ignored.)
 */
bool topic_sub(StringMap *topicMap, int socketFD, const char *topic) {
    struct TopicMapItem *item = stringmap_search(topicMap, (char *) topic);
    if (!item) {
        item = calloc(1, sizeof(struct TopicMapItem));
        item->socketFD = malloc(sizeof(int) * TOPIC_MAP_ITEM_START_CAPACITY);
        item->capacity = TOPIC_MAP_ITEM_START_CAPACITY;

        stringmap_add(topicMap, (char *) topic, item);
    }

    for (size_t i = 0; i < item->clientsN; i++) {
        if (item->socketFD[i] == socketFD) {
            return false;
        }
    }

    if (item->clientsN == item->capacity) {
        item->socketFD = realloc(item->socketFD,
                sizeof(int) * (item->capacity *= 2));
    }

    item->socketFD[item->clientsN++] = socketFD;

    return true;
}

/*
 * Unsubscribes the given client from the given topic.
 *
 * topicMap: Topic map.
 * socketFD: Client to unsubscribe.
 * topic: Topic for unsubscription.
 *
 * Returns: Was the unsubscription carried out (i.e. it wasn't ignored.)
 */
bool topic_unsub(StringMap *topicMap, int socketFD, const char *topic) {
    struct TopicMapItem *item = stringmap_search(topicMap, (char *) topic);
    if (!item) {
        return false;
    }

    for (size_t i = 0; i < item->clientsN; i++) {
        if (item->socketFD[i] == socketFD) {
            item->socketFD[i] = item->socketFD[--item->clientsN];
            return true;
        }
    }

    return false;
}

/*
 * Sends the given topic data to all subscribed clients.
 *
 * topicMap: Topic map.
 * name: Client publishing the data.
 * topic: Topic for publishing.
 * data: Data to publish.
 */
void topic_pub(StringMap *topicMap, const char *name, const char *topic,
        const char *data) {
    struct TopicMapItem *item = stringmap_search(topicMap, (char *) topic);
    if (!item) {
        return;
    }

    size_t msgSize = strlen(name) + strlen(topic) + strlen(data) + 4;
    char *msg = malloc(msgSize);
    msg[0] = '\0';
    strcat(msg, name);
    strcat(msg, ":");
    strcat(msg, topic);
    strcat(msg, ":");
    strcat(msg, data);
    strcat(msg, "\n");

    for (size_t i = 0; i < item->clientsN; i++) {
        send(item->socketFD[i], msg, msgSize, 0);
    }
}

/*
 * Parses the server command and returns it via an argument pointer.
 *
 * str: The command string to parse.
 * command: Place to store the parsed information into.
 */
void process_server_cmd(const char *str, struct ServerCMD *command) {
    command->type = SERVER_CMD_NONE;

    if (!strncmp(str, "name ", 5)) {
        str += 5;
        if (*str && !strchr(str, ' ') && !strchr(str, ':')) {
            command->type = SERVER_CMD_NAME;
            command->name = strdup(str);
        }
    } else if (!strncmp(str, "sub ", 4)) {
        str += 4;
        if (*str && !strchr(str, ' ') && !strchr(str, ':')) {
            command->type = SERVER_CMD_SUB;
            command->name = strdup(str);
        }
    } else if (!strncmp(str, "unsub ", 6)) {
        str += 6;
        if (*str && !strchr(str, ' ') && !strchr(str, ':')) {
            command->type = SERVER_CMD_UNSUB;
            command->name = strdup(str);
        }
    } else if (!strncmp(str, "pub ", 4)) {
        str += 4;
        const char *sep = strchr(str, ' ');
        if (sep) {
            for (const char *p = str; p != sep; p++) {
                if (*p == ':') {
                    return;
                }
            }

            if (*(sep + 1)) {
                command->type = SERVER_CMD_PUB;
                command->name = strndup(str, sep - str);
                command->value = strdup(sep + 1);
            }
        }
    }
}

/*
 * Executes the given server command: "name", "sub", "unsub" or "pub". It also
 * sends a failure message to the client if the command is invalid.
 *
 * arg: Client data.
 * serverCMD: Server command to execute.
 * name: Client name to set.
 */
void exec_server_cmd(struct ClientThreadData *threadData,
        struct ServerCMD *serverCMD, const char **name) {
    switch (serverCMD->type) {
        case SERVER_CMD_NONE:;
            char msg[] = ":invalid\n";
            send(threadData->socketFD, msg, sizeof(msg), 0);
            break;
        case SERVER_CMD_NAME:
            if (!*name) {
                *name = serverCMD->name;
            }
            break;
        case SERVER_CMD_SUB:
            if (*name) {
                pthread_mutex_lock(threadData->lockTopicMap);
                pthread_mutex_lock(&threadData->stats->lockModStats);
                threadData->stats->subOps += topic_sub(threadData->topicMap,
                        threadData->socketFD, serverCMD->name);
                pthread_mutex_unlock(&threadData->stats->lockModStats);
                pthread_mutex_unlock(threadData->lockTopicMap);
            }

            free(serverCMD->name);
            break;
        case SERVER_CMD_UNSUB:
            if (*name) {
                pthread_mutex_lock(threadData->lockTopicMap);
                pthread_mutex_lock(&threadData->stats->lockModStats);
                threadData->stats->unsubOps
                        += topic_unsub(threadData->topicMap,
                        threadData->socketFD, serverCMD->name);
                pthread_mutex_unlock(&threadData->stats->lockModStats);
                pthread_mutex_unlock(threadData->lockTopicMap);
            }

            free(serverCMD->name);
            break;
        case SERVER_CMD_PUB:
            if (*name) {
                pthread_mutex_lock(threadData->lockTopicMap);
                topic_pub(threadData->topicMap, *name, serverCMD->name,
                        serverCMD->value);
                pthread_mutex_lock(&threadData->stats->lockModStats);
                ++threadData->stats->pubOps;
                pthread_mutex_unlock(&threadData->stats->lockModStats);
                pthread_mutex_unlock(threadData->lockTopicMap);
            }

            free(serverCMD->name);
            free(serverCMD->value);
            break;
    }
}

/*
 * Opens a new listening socket on the supplied port and returns it. The server
 * should wait on this socket for client connections. On success the actual
 * port number is printed to STDERR.
 *
 * portNumber: Port number on which to open the socket. 0 for ephemeral port.
 *
 * Returns: A listening socket (main server socket).
 */
int open_listen(int portNumber) {
    // Open the listening socket.
    int socketFD = socket(AF_INET, SOCK_STREAM, 0);

    // If the port number is given, bind the main socket to it.
    if (portNumber) {
        struct sockaddr_in addr = {
            AF_INET,
            htons(portNumber),
            { INADDR_ANY },
        };

        bind(socketFD, (struct sockaddr *) &addr, sizeof(addr));
    }

    // Start to listen on the main socket.
    if (listen(socketFD, LISTENING_BACKLOG) == -1) {
        close(socketFD);
        error(EXIT_CODE_LIST_FAIL, 0, ERROR_MSG_LIST_FAIL);
    }

    // Get and print the actual port number.
    struct sockaddr_in addr;
    socklen_t addrLen = sizeof(addr);
    getsockname(socketFD, (struct sockaddr *) &addr, &addrLen);
    fprintf(stderr, "%u\n", ntohs(addr.sin_port));

    return socketFD;
}

/*
 * Formats and returns the program statistics as a user-friendly string. You
 * must free the returned data.
 *
 * stats: Pointer to program statistics.
 *
 * Returns: User-frindly formatted program statistics.
 */
char *stats_to_string(const struct Stats *stats) {
#define STATS_STRING    "Connected clients: %lu\n"  \
                        "Completed clients: %lu\n"  \
                        "sub operations: %lu\n"     \
                        "unsub operations: %lu\n"   \
                        "pub operations: %lu\n"

    int strLen = snprintf(NULL, 0, STATS_STRING, stats->connNow,
            stats->connTotal, stats->subOps, stats->unsubOps, stats->pubOps);

    char *str = malloc(strLen + 1);

    snprintf(str, strLen + 1, STATS_STRING, stats->connNow, stats->connTotal,
            stats->subOps, stats->unsubOps, stats->pubOps);

#undef STATS_STRING

    return str;
}

/*
 * Prints out the program statistics to STDERR when the program receives SIGHUP
 * signal. Mutex is used when printing info to ensure maximum precision.
 *
 * arg: Program statistics.
 */
void *stats_sighup(void *arg) {
    struct Stats *stats = arg;

    sigset_t sigs;
    sigemptyset(&sigs);
    sigaddset(&sigs, SIGHUP);

    for (int sig; !sigwait(&sigs, &sig);) {
        pthread_mutex_lock(&stats->lockModStats);
        char *str = stats_to_string(stats);
        pthread_mutex_unlock(&stats->lockModStats);
        fprintf(stderr, "%s", str);
        free(str);
    }

    return NULL;
}

/*
 * Manages a single HTTP connection, over which it sends program statistics.
 * Terminates the connection on an invalid HTTP request. Run it within a
 * thread.
 *
 * arg: HTTP server data.
 */
void *stats_http_client(void *arg) {
    struct HTTPData *httpData = arg;
    FILE *socketFile = fdopen(httpData->socketFD, "r+");

    char *method; char *address; HttpHeader **headers; char *body;
    for (bool run = true; run && get_HTTP_request(socketFile, &method,
            &address, &headers, &body);) {
        if (!strcmp(method, "GET") && !strcmp(address, "/stats")) {
            pthread_mutex_lock(&httpData->stats->lockModStats);
            char *body = stats_to_string(httpData->stats);
            pthread_mutex_unlock(&httpData->stats->lockModStats);

            size_t bodyLen = strlen(body);
            int contentLenLen = snprintf(NULL, 0, "%lu", bodyLen);
            char contentLen[contentLenLen + 1];
            snprintf(contentLen, contentLenLen + 1, "%lu", bodyLen);

            HttpHeader header = { "Content-Length", contentLen };
            HttpHeader *headers[] = { &header, NULL };

            char *httpRes = construct_HTTP_response(200, "(OK)", headers,
                    body);

            free(body);
            fprintf(socketFile, "%s", httpRes);
            free(httpRes);
        } else {
            run = false;
        }

        free(method);
        free(address);
        free_array_of_headers(headers);
        free(body);
    }

    fclose(socketFile);
    free(httpData);

    return NULL;
}

/*
 * Starts the HTTP server at the specified service name / port number. The HTTP
 * server serves the requests for connections by spawning client threads, which
 * in turn serve program statistics to the client. Run it within a thread.
 *
 * arg: Pointer to the program statistics.
 */
void *stats_http_server(void *arg) {
    struct Stats *stats = arg;
    const char *port = getenv("A4_HTTP_PORT");

    // Discover the HTTP port.
    struct addrinfo *serverAddr;
    struct addrinfo serverSpec = { .ai_family = AF_INET,
            .ai_socktype = SOCK_STREAM, .ai_protocol = 0 };
    if (getaddrinfo("localhost", port, &serverSpec, &serverAddr)) {
        freeaddrinfo(serverAddr);
        error(EXIT_CODE_HTTP_LIST_FAIL, 0, ERROR_MSG_HTTP_LIST_FAIL);
    }

    // Open the listening socket and bind it to the HTTP port.
    int socketFD = socket(AF_INET, SOCK_STREAM, 0);
    bind(socketFD, serverAddr->ai_addr, serverAddr->ai_addrlen);
    freeaddrinfo(serverAddr);

    // Start to listen on the main socket.
    if (listen(socketFD, LISTENING_BACKLOG) == -1) {
        close(socketFD);
        error(EXIT_CODE_HTTP_LIST_FAIL, 0, ERROR_MSG_HTTP_LIST_FAIL);
    }

    while (true) {
        struct sockaddr_in addr;
        socklen_t addrLen = sizeof(addr);
        int clientSocketFD = accept(socketFD, (struct sockaddr *) &addr,
                &addrLen);

        struct HTTPData *httpData = malloc(sizeof(struct HTTPData ));
        httpData->socketFD = clientSocketFD;
        httpData->stats = stats;

        pthread_t clientThread;
        pthread_create(&clientThread, NULL, &stats_http_client, httpData);
    }

    return NULL;
}

/*
 * Initialize the data structure for reporting program statistics and its
 * helper variables. Sets up the handlers for statistics reporting.
 *
 * stats: Pointer to the program statistics.
 */
void stats_init(struct Stats *stats) {
    // Initialize the data structures.
    memset(stats, 0, sizeof(*stats));
    pthread_mutex_init(&stats->lockModStats, NULL); 

    // Block SIGHUP for the current (and therefore all child) threads.
    sigset_t sigs;
    sigemptyset(&sigs);
    sigaddset(&sigs, SIGHUP);
    pthread_sigmask(SIG_BLOCK, &sigs, NULL);

    // Setup the SIGHUP handler so that it prints out the statistics in the
    // dedicated thread.
    pthread_t threadStats;
    pthread_create(&threadStats, NULL, &stats_sighup, stats);

    // Optionally also start the HTTP server for displaying statistics.
    if (getenv("A4_HTTP_PORT")) {
        pthread_t threadHTTP;
        pthread_create(&threadHTTP, NULL, &stats_http_server, stats);
    }
}

/*
 * Releases all resources associated with a client thread.
 *
 * threadData: Client data.
 */
void client_data_free(struct ClientThreadData *threadData) {
    pthread_mutex_lock(&threadData->stats->lockModStats);
    if (threadData->stats->connNow-- == threadData->connectionsN
            && threadData->connectionsN) {
        pthread_mutex_unlock(threadData->lockAccConn);
    }
    pthread_mutex_unlock(&threadData->stats->lockModStats);

    pthread_mutex_lock(threadData->lockTopicMap);

    close(threadData->socketFD);

    for (StringMapItem *item = stringmap_iterate(threadData->topicMap, NULL);
            item; item = stringmap_iterate(threadData->topicMap, item)) {
        struct TopicMapItem *topic = item->item;
        for (int i = 0; i < topic->clientsN; i++) {
            if (topic->socketFD[i]  == threadData->socketFD) {
                topic->socketFD[i] =
                        topic->socketFD[--topic->clientsN];
                break;
            }
        }
    }

    pthread_mutex_unlock(threadData->lockTopicMap);
}

/*
 * Handles a particular client connection: it is responsible for executing
 * client commands and sending it server responses.
 *
 * arg: Client data.
 */
void *client_run(void *arg) {
    struct ClientThreadData *threadData = arg;

    char data[NETWORK_BUFFER_SIZE]; size_t countRecv; char *command = NULL;
    size_t commandLen = 0; char *name = NULL;
    while ((countRecv = recv(threadData->socketFD, data, sizeof(data) - 1, 0))
            >= 1) {
        data[countRecv] = '\0';

        char *newData = data;
        for (char *p = strchr(data, '\n'); p; p = strchr(p, '\n')) {
            *p = '\0';
            size_t newDataLen = p - newData;
            countRecv -= newDataLen + 1;

            command = realloc(command, 1 + commandLen + newDataLen);
            command[commandLen] = '\0';

            strncat(command, newData, newDataLen);
            commandLen += newDataLen;

            struct ServerCMD serverCMD;
            process_server_cmd(command, &serverCMD);
            exec_server_cmd(threadData, &serverCMD, (const char **) &name);

            free(command);
            command = NULL;
            commandLen = 0;

            newData = ++p;
        }

        command = realloc(command, 1 + commandLen + countRecv);
        command[commandLen] = '\0';

        strncat(command, newData, countRecv);
        commandLen += countRecv;
    }

    free(command);
    free(name);

    client_data_free(threadData);

    return NULL;
}

/*
 * Waits for incoming connections from the clients on the supplied socket. On
 * success, a new thread is spawned to handle that particular client. If
 * connection limit is reached, that particular client's connection attempt
 * will be stalled until a new connection becomes available again. Exits the
 * program on error. It never returns.
 *
 * socketFD: Main server socket.
 * connectionsN: Maximum allowed number of simultanous client connections.
 * stats: Program statistics.
 */
void client_connect(int socketFD, int connectionsN, struct Stats *stats) {
    StringMap *topicMap = stringmap_init();

    pthread_mutex_t lockTopicMap, lockAccConn;
    pthread_mutex_init(&lockTopicMap, NULL);
    pthread_mutex_init(&lockAccConn, NULL);

    while (true) {
        pthread_mutex_lock(&lockAccConn);

        struct sockaddr_in addr;
        socklen_t addrLen = sizeof(addr);
        int clientSocketFD = accept(socketFD, (struct sockaddr *) &addr,
                &addrLen);

        pthread_mutex_lock(&stats->lockModStats);
        ++stats->connTotal;
        ++stats->connNow;
        if (!connectionsN || stats->connNow < connectionsN) {
            pthread_mutex_unlock(&lockAccConn);
        }
        pthread_mutex_unlock(&stats->lockModStats);

        struct ClientThreadData *clientData = malloc(
                sizeof(struct ClientThreadData));

        clientData->socketFD = clientSocketFD;
        clientData->topicMap = topicMap;
        clientData->connectionsN = connectionsN;
        clientData->stats = stats;
        clientData->lockTopicMap = &lockTopicMap;
        clientData->lockAccConn = &lockAccConn;

        pthread_t clientThread;
        pthread_create(&clientThread, NULL, &client_run, clientData);
    }
}

int main(int argc, char *argv[]) {
    // Parse the command-line arguments and store them in the dedicated
    // data structure.
    struct CMDArgs cmdArgs;
    process_cmd_args(&cmdArgs, argc, (const char *const *) argv);

    // Open the listening socket, on which the clients should attempt to
    // connect.
    int socketFD = open_listen(cmdArgs.portNumber);

    // Set up the program statistics reporting.
    struct Stats stats;
    stats_init(&stats);

    // Wait for and handle client connections, taking care of the connection
    // limit.
    client_connect(socketFD, cmdArgs.connectionsN, &stats);
}
