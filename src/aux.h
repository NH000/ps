/*
 * vim: set cc=80 et ts=4 sts=0 sw=0:
 *
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of PS.
 *
 * PS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PS.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AUX_H
#define AUX_H

#include <stdbool.h>

/*
 * Concatenates the given strings and returns the result. The caller must free
 * the returned string.
 *
 * strings_n: Number of strings to concatenate.
 * ...: Strings for concatenation.
 *
 * Returns: Concatenated strings, or NULL on memory allocation error.
 *
 * Errors: Failure to allocate the resulting string, signified by NULL return
 * value.
 */
char *string_concat_multiple(int strings_n, ...);

/*
 * Test whether the entire given is string is made up of digits.
 *
 * str: String to test.
 *
 * Returns: Whether the entire given string is made up of digits. If `str` is
 *          NULL or empty always returns `false`.
 */
bool is_all_digits(const char *str);

#endif
